# Add Rivets adapter to work with our models.
Rivets.adapters[":"] =
	observe: (obj, keypath, callback) ->
		#console.log "Rivets observe :: keypath: \"#{keypath}\" obj: \"#{obj}\""
		obj.on "change:" + keypath, ->
			val = obj.get keypath
			callback val
		return undefined
	unobserve: (obj, keypath, callback) ->
		#console.log "Rivets unobserve :: keypath: \"#{keypath}\" obj: \"#{obj}\""
		obj.off "change:" + keypath, callback
		return undefined
	get: (obj, keypath) ->
		#console.log "Rivets get :: keypath: \"#{keypath}\" obj: \"#{obj}\""
		return obj.get keypath
	set: (obj, keypath, value) ->
		#console.log "Rivets set :: keypath: \"#{keypath}\" obj: \"#{obj}\" value: \"#{value}\""
		obj.set keypath, value
		return undefined

RivetsHelper = Antifreeze.RivetsHelper =
	# Auto-discovers all relevant elements on the view and attaches the model to them via Rivets.
	bind: () ->
		console.log "Antifreeze.RivetsHelper bind", @
		view = @view
		convertData = @helper.convertData
		# Prepare data
		#data = convertData view.model()
		# Bind.
		if @rivetsView
			@rivetsView.unbind()
		@rivetsView = Rivets.bind view.element(), view.model()

	# Prepares the model data in a format consumable by Rivets.
	# For all references, Rivets requires the initial level to be a plain object, thus single-level references are not
	# possible.
	convertData: (model) ->
		throw new Error "Model must be a Model" unless model instanceof Discrete.Model
		data = {}
		for k in model.keys()
			data[k] = model.get k
		return data
